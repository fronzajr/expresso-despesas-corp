import { CategoriaDespesa } from './../../pages/categoria-despesa/categoria-despesa';
import { NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

/**
 * Generated class for the DespesaDetalheComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'despesa-detalhe',
  templateUrl: 'despesa-detalhe.html'
})
export class DespesaDetalheComponent {

  data: CategoriaDespesa;

  constructor(public navParams:NavParams) {
    
    if (this.navParams.data) {
      this.data = this.navParams.data;
    }
  }

}
