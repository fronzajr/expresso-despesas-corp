import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DespesaGeralComponent } from './despesa-geral/despesa-geral';
import { DespesaAnaliseComponent } from './despesa-analise/despesa-analise';
import { DespesaDetalheComponent } from './despesa-detalhe/despesa-detalhe';

@NgModule({
    declarations: [DespesaGeralComponent, DespesaAnaliseComponent, DespesaDetalheComponent],
    imports: [IonicModule],
    exports: [DespesaGeralComponent, DespesaAnaliseComponent, DespesaDetalheComponent]
})
export class ComponentsModule {
}
