import { CategoriaDespesa } from './../../pages/categoria-despesa/categoria-despesa';
import { NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

/**
 * Generated class for the DespesaAnaliseComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'despesa-analise',
  templateUrl: 'despesa-analise.html'
})
export class DespesaAnaliseComponent {

  public data: CategoriaDespesa;

  constructor(public navParams: NavParams) {
    if (this.navParams.data) {
      this.data = this.navParams.data;

    }
  }

}
