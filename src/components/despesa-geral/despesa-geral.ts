import { NavParams } from 'ionic-angular';
import { CategoriaDespesa } from './../../pages/categoria-despesa/categoria-despesa';
import { Component } from '@angular/core';

@Component({
  selector: 'despesa-geral',
  templateUrl: 'despesa-geral.html'
})
export class DespesaGeralComponent {
  
  public data: CategoriaDespesa;
  
  public pagamentos : Array<String> = [
    'Dinheiro',
    'Débito',
    'Crédito',
    'Outro'
  ]

  constructor(public navParams: NavParams) {
    if (this.navParams.data) {
      this.data = this.navParams.data;

      if (this.data && this.data.model) {
        this.data.model.geral = {
          data: new Date(),
          hora: new Date(),
          descricao: '',
          total: 0,
          pago: ''
        }
      }
    }
  }

}
