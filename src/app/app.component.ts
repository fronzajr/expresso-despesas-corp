import { DespesasPage } from './../pages/despesas/despesas';
import { SankhyaService } from './../providers/sankhya-service';
import { IonicDB } from '../providers/ionic-db';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CategoriaDespesaPage } from '../pages/categoria-despesa/categoria-despesa';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { IonDigitKeyboardCmp } from '../components/ion-digit-keyboard';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = DespesasPage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public sankhya: SankhyaService, public storage: Storage, public splashScreen: SplashScreen, public db: IonicDB) {
    //this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'Categoria Despesa', component: CategoriaDespesaPage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(async () => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      const userID = await this.storage.get("userID");

      if (userID) { 
        this.rootPage = HomePage;
      } else {
        this.rootPage = LoginPage; // LoginPage;
      }

      await this.db.connect({
        dbname: "despesas.db"
      });

      try {

        // const created = await this.db.execute(`
        //   CREATE TABLE IF NOT EXISTS example (
        //     id	INTEGER PRIMARY KEY AUTOINCREMENT,
        //     name	DATE NOT NULL UNIQUE,
        //     birthday	DATE NOT NULL)
        // `);

        // console.log(created);

        // await this.db.execute("INSERT INTO example (name, birthday) VALUES ('Teste " + Date.now() + "', '2018-09-12 16:26:00')");

        // const response = await this.db.queryAll("SELECT * FROM example", []);

        // deveria funcionar, o problema é CORS
        const response = this.sankhya.login("JUNIOR", "123456");
        console.log("login response:", response);

      } catch (err) {
        console.error(err);
      }
       
    });

    
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
