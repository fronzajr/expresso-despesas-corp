import { DespesasPage } from './../pages/despesas/despesas';
import { SQLite } from '@ionic-native/sqlite';
import { IonicStorageModule } from "@ionic/storage";
import { DespesaDetalheComponent } from '../components/despesa-detalhe/despesa-detalhe';
import { DespesaGeralComponent } from '../components/despesa-geral/despesa-geral';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoriaDespesaPage } from '../pages/categoria-despesa/categoria-despesa';
import { LancarDespesaPage } from '../pages/lancar-despesa/lancar-despesa';
import { DespesaAnaliseComponent } from '../components/despesa-analise/despesa-analise';
import { IonicDB } from '../providers/ionic-db';
import { ComponentsModule } from '../components/components.module';
import { LoginPage } from '../pages/login/login';
import { SankhyaService } from '../providers/sankhya-service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { NovaDespesaPage } from '../pages/nova-despesa/nova-despesa';
import { IonDigitKeyboard } from '../components/ion-digit-keyboard/ion-digit-keyboard.module';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localept from '@angular/common/locales/pt';

registerLocaleData(localept, 'pt');

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    CategoriaDespesaPage,
    LancarDespesaPage,
    DespesasPage,
    NovaDespesaPage
  ],
  imports: [
    IonDigitKeyboard,
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      driverOrder: ['sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ListPage,
    CategoriaDespesaPage,
    LancarDespesaPage,
    DespesasPage,
    NovaDespesaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SQLite,
    IonicDB,
    SankhyaService,
    {provide: LOCALE_ID, useValue: 'pt-BR'}
  ]
})
export class AppModule { }
