import { Platform } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

declare global {
  interface Window {
    sqlitePlugin: any;
  }
}

export interface IDbConnectionSettings {
  dbname: string;
  location?: string;
}

@Injectable()
export class IonicDB {
  private _db: any;

  constructor(private sqlite: SQLite, private plt: Platform) {
  }

  async connect(settings: IDbConnectionSettings) {

    if (settings === undefined || settings.dbname === undefined) {
      console.error("Storage: Failed to open database, empty name");
      return;
    }

    if (this.plt.is('cordova') && window.sqlitePlugin) {
      this._db = window.sqlitePlugin.openDatabase({
        name: settings.dbname,
        location: 'default'
      });
    } else {
      console.warn('Storage: SQLite plugin not installed, falling back to WebSQL. Make sure to install cordova-sqlite-storage in production!');
      this._db = window['openDatabase'](settings.dbname, '1.0', 'database', 60 * 1024 * 1024);
    }
  }

  queryAll(query: string, params: any[] = []): Promise<any> {
    return this.execute(query, params).then(data => {
      if (data.res.rows.length > 0) {
        console.log('Rows found.');
        if (this.plt.is('cordova') && window.sqlitePlugin) {
          let result = [];
          for (let i = 0; i < data.res.rows.length; i++) {
            var row = data.res.rows.item(i);
            result.push(row);
          }
          return result;
        }
        else {
          return data.res.rows;
        }
      }
    });
  }

  execute(query: string, params: any[] = []): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this._db.transaction(function sqlTransactionCallback(tx) {
          tx.executeSql(query, params, (tx: any, res: any) => {
            resolve({ tx: tx, res: res });
          }, (tx: any, err: any) => {
            reject({ tx: tx, err: err });
            return false;
          });
        }, function transationErrorCallback(err) {
          reject({ err: err })
        });
      } catch (err) {
        reject({ err: err });
      }
    });
  }

}
