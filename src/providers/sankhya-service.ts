import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import xml2js from 'xml2js';

export interface ServiceCall {
  service: string;
  params?: any;
  timeout?: number;
  throwsError?: boolean;
}

enum ServiceStatus {
  STATUS_ERROR = '0',
  STATUS_OK = '1',
  STATUS_INFO = '2',
  STATUS_TIMEOUT = '3',
  STATUS_SERVICE_CANCELED = '4'
}

const APP_NAME = "Sankhya";
const DEFAULT_MODULE = 'mge';
//const DEFAULT_SERVICE_TIMEOUT = 30000;

//const fallbackErrorShowing = false;
const serviceUrlTemplate = '/{0}/service.sbr?serviceName={1}&counter={2}&application={3}&allowConcurrentCalls=true';

@Injectable()
export class SankhyaService {
  private counter: number = 0;
  private serverUrl: string = "http://mge.teleworld.com.br";

  constructor(public http: HttpClient, public storage: Storage) {
  }

  public async login(username: string, password: string) {
    return await this.callService({
      service: 'MobileLoginSP.login',
      timeout: 60,
      params: {
        NOMUSU: username,
        INTERNO: password
      }
    });
  }

  private parseXML<T>(xml: string): Promise<T> {
    return new Promise((resolve, reject) => {
      xml2js.parseString(xml, (err, result) => {
        if (err) {
          return reject(err);
        }
        resolve(result);
      })
    });
  }

  private async callService(options: ServiceCall) {

    var service = options.service;
    var params = options.params;
    var timeout = options.timeout ? options.timeout : 30;
    var throwsErrors = options.throwsError == null || options.throwsError == true;
    var serverAddress = this.serverUrl;

    var module = DEFAULT_MODULE;
    var serviceName = service;

    if (service.indexOf('@') > -1) {
      var s = service.split('@');
      module = s[0];
      serviceName = s[1];
    }

    var url = serviceUrlTemplate.replace('{0}', module);
    url = url.replace('{1}', serviceName);
    url = url.replace('{2}', (this.counter++).toString());
    url = url.replace('{3}', APP_NAME);

    var mgeSession = await this.storage.get("sessionID");

    if (mgeSession != null) {
      url += '&mgeSession=' + mgeSession;
    }

    const requestHeaders = new HttpHeaders({
      'Content-Type': 'text/xml'
    });

    var kID = await this.storage.get('kID');

    if (kID != null) {
      requestHeaders.set('Authorization', 'Bearer ' + kID);
    }

    var requestBodyObj = {
      serviceRequest: {
        _serviceName: serviceName,
        requestBody: {}
      }
    };

    if (params != null) {
      requestBodyObj.serviceRequest.requestBody = params;
    }

    var builder = new xml2js.Builder();
    var xmlData = builder.buildObject(requestBodyObj);

    var canceller = Promise.resolve();

    try {
      const response = await this.http.post(serverAddress + url, xmlData, {
        headers: requestHeaders,
        responseType: 'text'
      }).toPromise();

      const data = await this.parseXML<any>(response);
      console.log("Response Data:", data);

      var serverStatus = data.serviceResponse._status;
      var serverMsg = null;

      if (data.serviceResponse.statusMessage != null) {
        var msg = atob(data.serviceResponse.statusMessage.__cdata);
        serverMsg = msg;
      }

      if (serverStatus == ServiceStatus.STATUS_TIMEOUT) {
        // _redirectToLogin();
        console.log("redirect to login!")
      }
      else if (serverStatus == ServiceStatus.STATUS_ERROR) {
        console.log(serverMsg, status, data);
        throw new Error(serverMsg + "(" + status + ")");
      }
      else if (serverStatus == ServiceStatus.STATUS_SERVICE_CANCELED) {
        console.error("cancelled", serverMsg, status, data);
        throw new Error("cancelled: " + serverMsg + "(" + status + ")");
      }
      else {
        return data.serviceResponse.responseBody;
      }

    } catch (error) {
      console.log('Connection Error: ', error);
      throw error;

      // TODO: implementar countdown e requisitar novamente...
      // this.fallbackError(originalRequest);
    }
  }

}
