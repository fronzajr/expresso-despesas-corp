import { IonDigitKeyboardOptions } from './../../components/ion-digit-keyboard/interfaces/ion-digit-keyboard.interface';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-nova-despesa',
  templateUrl: 'nova-despesa.html',
})
export class NovaDespesaPage {

  public valor: string = "0";
  public titulo: string

  public keyboardSettings: IonDigitKeyboardOptions = {
    align: 'center',
    //width: '85%',
    visible: true,
    leftActionOptions: {
      iconName: 'ios-backspace-outline',
      fontSize: '1.4em'
    },
    rightActionOptions: {
      text: '.',
      fontSize: '1.3em'
    },
    roundButtons: false,
    showLetters: false,
    swipeToHide: false,
    theme: 'ionic'
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    if (this.navParams.data) {
      this.titulo = this.navParams.data;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NovaDespesaPage');
  }

  public numberClick(event: number) {
    console.log(event);
    this.valor += event.toString();
  }

  leftActionClick() {
    if (this.valor.length > 0) {
      if (this.valor.endsWith('.')) {
        this.valor = this.valor.substr(0, this.valor.length - 2);
      } 
      else if (this.valor == '0') {
      }
      else {
        this.valor = this.valor.substr(0, this.valor.length - 1);
      }
    } else {
      this.valor = "0";
    }
  }

  rightActionClick() {
    if (this.valor.indexOf('.') == -1) {
      this.valor += '.';
    }
    console.log("cadastrar despesa");
  }
}
