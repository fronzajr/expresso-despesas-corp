import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ListPage } from '../list/list';
import { LancarDespesaPage } from '../lancar-despesa/lancar-despesa';
import { Page } from 'ionic-angular/umd/navigation/nav-util';

export interface CategoriaModel {
  geral: {
    descricao?: string,
    data?: Date,
    hora?: Date,
    total?: number,
    pago?: any
  },
  detalhe: any,
  analise: {
    cliente?: any,
    projeto?: any,
    centroCusto?: any,
    reembosavel?: boolean,
    clienteACobrar?: boolean,
    comentario?: string
  }
}

export interface CategoriaDespesa {
  icon: string,
  title: string,
  page: Page,
  detail: string,
  img?: boolean,
  model?: CategoriaModel
}

@Component({
  selector: 'page-categoria-despesa',
  templateUrl: 'categoria-despesa.html',
})
export class CategoriaDespesaPage {

  despesas: Array<CategoriaDespesa>;

  navigateTo(menu: CategoriaDespesa) {
    //this.navCtrl.push(menu.page);
    this.navRoot.push(menu.page, menu);
  }

  constructor(public navParams: NavParams, public navRoot: NavController) {

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriaDespesaPage');
    this.despesas = [
      {
        icon: 'assets/imgs/rent-car.png',
        img: true,
        title: 'Aluguel de carro',
        page: LancarDespesaPage,
        detail: 'carro'
      },
      {
        icon: 'assets/imgs/air-freight.png',
        img: true,
        title: 'Avião',
        page: LancarDespesaPage,
        detail: 'aviao'
      },
      {
        icon: 'assets/imgs/gas-pump.png',
        img: true,
        title: 'Combustível',
        page: LancarDespesaPage,
        detail: 'combustivel'
      },
      {
        icon: 'assets/imgs/icon.png',
        img: true,
        title: 'Diversos',
        page: LancarDespesaPage,
        detail: 'diversos'
      },
      {
        icon: 'assets/imgs/parking-sign.png',
        img: true,
        title: 'Estacionamento',
        page: LancarDespesaPage,
        detail: 'diversos'
      },
      {
        icon: 'assets/imgs/hotel.png',
        img: true,
        title: 'Hotel',
        page: LancarDespesaPage,
        detail: 'hotel'
      },
      {
        icon: 'assets/imgs/wifi.png',
        img: true,
        title: 'Internet',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/payment-method.png',
        img: true,
        title: 'Pagamento adiantado',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/toll-road.png',
        img: true,
        title: 'Pedágio',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/gauge.png',
        img: true,
        title: 'Quilometragem',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/route.png',
        img: true,
        title: 'Rastreamento do tempo',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/restaurant.png',
        img: true,
        title: 'Restaurante',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/taxi.png',
        img: true,
        title: 'Taxi',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/phone-call.png',
        img: true,
        title: 'Telefone',
        page: LancarDespesaPage,
        detail: 'internet'
      },
      {
        icon: 'assets/imgs/bus.png',
        title: 'Transporte publico',
        page: LancarDespesaPage,
        detail: 'internet',
        img: true
      },
      {
        icon: 'assets/imgs/train-public-transport.png',
        title: 'Trem',
        page: LancarDespesaPage,
        detail: 'internet',
        img: true
      }
    ];

  }

}
