import { CategoriaDespesa } from './../categoria-despesa/categoria-despesa';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LancarDespesaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lancar-despesa',
  templateUrl: 'lancar-despesa.html',
})
export class LancarDespesaPage {
  categoria: CategoriaDespesa;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if (this.navParams.data) {
      this.categoria = this.navParams.data;
      this.categoria.model = {
        analise: {},
        detalhe: {},
        geral: {}
      };
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LancarDespesaPage');
  }

}
